#include "StableFluidsEnsemble.h"

StableFluidsEnsemble::StableFluidsEnsemble()
: m_fluid_sim(64,64)
, m_bgcolor(0.0,0.0,0.0)
, m_colormap(Colormap::MATLAB_JET,256)
, m_window_width(4*m_fluid_sim.physicalCols())
, m_window_height(4*m_fluid_sim.physicalRows())
, m_left_drag(false)
, m_right_drag(false)
, m_last_row(0)
, m_last_col(0)
{}

StableFluidsEnsemble::~StableFluidsEnsemble()
{}

void StableFluidsEnsemble::stepSystem( const scalar& dt )
{
  m_fluid_sim.stepSystem(dt);
}

void StableFluidsEnsemble::keyboard( const unsigned char& key, const int& x, const int& y )
{
  if( key == '=' || key == '+' )
  {
    m_colormap.incrementColormap();
  }
  if( key == '-' || key == '_' )
  {
    m_colormap.decrementColormap();
  }
  if( key == 'r' || key == 'R' )
  {
    m_fluid_sim.clear();
  }
}

void StableFluidsEnsemble::special( const int& key, const int& x, const int& y )
{}

void StableFluidsEnsemble::mouse( const int& button, const int& state, const int& x, const int& y )
{
  const int& row = (y*m_fluid_sim.physicalRows())/m_window_height;
  const int& col = (x*m_fluid_sim.physicalCols())/m_window_width;

  // Record that the left mouse button was pressed (begin left drag)
  if( !m_right_drag && button == GLUT_LEFT_BUTTON && state == GLUT_DOWN )
  {
    m_left_drag = true;
    m_last_row = row;
    m_last_col = col;
  }
  // Record that the left mouse button was released (end left drag)
  if( button == GLUT_LEFT_BUTTON && state == GLUT_UP )
  {
    m_left_drag = false;
    m_last_row = 0;
    m_last_col = 0;
  }

  // Record that the right mouse button was pressed (begin right drag)
  if( !m_right_drag && button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN )
  {
    m_right_drag = true;
    m_last_row = row;
    m_last_col = col;
  }
  // Record that the right mouse button was released (end right drag)
  if( button == GLUT_RIGHT_BUTTON && state == GLUT_UP )
  {
    addSphere(row,col,4);
    m_right_drag = false;
    m_last_row = 0;
    m_last_col = 0;
  }  
}

void StableFluidsEnsemble::motion( const int& x, const int& y ) 
{
  const int& row = (y*m_fluid_sim.physicalRows())/m_window_height;
  const int& col = (x*m_fluid_sim.physicalCols())/m_window_width;
  
  // If a left button drag is in progress
  if( m_left_drag )
  {
    // Points returned by glut could be far apart, so connect with
    //  an unrboken line.
    std::vector<int> xpts;
    std::vector<int> ypts;
    renderingutils::bressenhamLine( m_last_row, m_last_col, row, col, xpts, ypts );
    assert( xpts.size() == ypts.size() );

    m_last_row = row;
    m_last_col = col;

    // If there is only one point, we can't define a direction for the force
    if( xpts.size() <= 1 ) return;

    // Compute a vector connecting the two points returned by glut
    Vector2s dir;
    dir << ((scalar)xpts.back())-((scalar)xpts.front()), ((scalar)ypts.back())-((scalar)ypts.front());
    scalar norm = dir.norm();
    if( norm == 0 ) return;
    dir /= norm;
    // Make the vector magnitude 2
    dir *= 2.0;

    // Add scaled endpoint vectors to each point on the line
    ArrayXs& u = m_fluid_sim.getVerticalVelocities();
    ArrayXs& v = m_fluid_sim.getHorizontalVelocities();
    for( std::vector<int>::size_type i = 0; i < xpts.size(); ++i )
    {
      if( xpts[i] < 0 ) continue;
      if( xpts[i] >= m_fluid_sim.physicalRows() ) continue;
      if( ypts[i] < 0 ) continue;
      if( ypts[i] >= m_fluid_sim.physicalCols() ) continue;

      u(xpts[i]+1,ypts[i]+1) += dir.x();
      v(xpts[i]+1,ypts[i]+1) += dir.y();
    }
  }

  // If a right button drag is in progress
  if( m_right_drag )
  {
    // Points returned by glut could be far apart, so connect with
    //  an unrboken line.
    std::vector<int> xpts;
    std::vector<int> ypts;
    renderingutils::bressenhamLine( m_last_row, m_last_col, row, col, xpts, ypts );
    assert( xpts.size() == ypts.size() );
    
    // Place a sphere at each point on the line
    for( std::vector<int>::size_type i = 0; i < xpts.size(); ++i ) addSphere(xpts[i],ypts[i],4);

    m_last_row = row;
    m_last_col = col;
  }
}

void StableFluidsEnsemble::initializeOpenGL()
{
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
  glClearColor((GLdouble)m_bgcolor.r(),(GLdouble)m_bgcolor.g(),(GLdouble)m_bgcolor.b(),(GLdouble)1.0);
}

void StableFluidsEnsemble::reshape( const int& w, const int& h ) 
{
  m_window_width = w;
  m_window_height = h;

	glViewport(0,0,m_window_width,m_window_height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, m_fluid_sim.physicalRows(), 0, m_fluid_sim.physicalCols());

  // Ensure that the window is square
  if( m_window_width != m_window_height )
  {
    glutReshapeWindow(std::max(m_window_width,m_window_height),std::max(m_window_width,m_window_height));
  }
  // Ensure that the window is at least as big as the number of fluid cells
  if( m_window_width < m_fluid_sim.physicalRows() )
  {
    glutReshapeWindow(m_fluid_sim.physicalRows(),m_fluid_sim.physicalRows());
  }
}

void StableFluidsEnsemble::display()
{
  const ArrayXs& marker_density = m_fluid_sim.getMarkerDensities();

  for( int row = 0; row < m_fluid_sim.physicalRows(); ++row ) for( int col = 0; col < m_fluid_sim.physicalCols(); ++col )
  {  
    // Compute the color of the current grid cell
    scalar r,g,b;
    m_colormap.getColorByDensity( marker_density(row+1,col+1), r, g, b );
    glColor3d((GLdouble)r,(GLdouble)g,(GLdouble)b);

    // Compute the left and right endpoints of the cell
    const scalar& xl = col;
    const scalar& xr = col+1;
    assert( xl <= xr );
    
    // Compute the bottom and top endpoints of the cell
    const scalar& yb = m_fluid_sim.physicalRows()-row-1;
    const scalar& yu = m_fluid_sim.physicalRows()-row;
    assert( yb <= yu );
    
    glBegin(GL_QUADS);
      glVertex2d((GLdouble)xl,(GLdouble)yb);
      glVertex2d((GLdouble)xr,(GLdouble)yb);
      glVertex2d((GLdouble)xr,(GLdouble)yu);
      glVertex2d((GLdouble)xl,(GLdouble)yu);
    glEnd();
  }    
}

unsigned int StableFluidsEnsemble::getGlutDisplayMode() const
{
  return GLUT_DOUBLE|GLUT_RGBA;
}

const int& StableFluidsEnsemble::getWindowWidth() const
{
  return m_window_width;
}

const int& StableFluidsEnsemble::getWindowHeight() const
{
  return m_window_height;
}

const renderingutils::Color& StableFluidsEnsemble::getBackgroundColor() const
{
  return m_bgcolor;
}

void StableFluidsEnsemble::addSphere( const int& row, const int& col, const int& R )
{
  const scalar& scalarR = (scalar) R;
  
  ArrayXs& marker_densities = m_fluid_sim.getMarkerDensities();
  
  for( int i = -R; i <= R; ++i ) for( int j = -R; j <= R; ++j )
  {
    // If the sphere extends off the grid, nothing to do
    if( row+i < 0 ) continue;
    if( row+i >= m_fluid_sim.physicalRows() ) continue;
    if( col+j < 0 ) continue;
    if( col+j >= m_fluid_sim.physicalCols() ) continue;

    assert( row+i >= 0 ); assert( row+i < m_fluid_sim.physicalRows() );
    assert( col+j >= 0 ); assert( col+j < m_fluid_sim.physicalCols() );
    
    // Compute the squared distance from the center
    const scalar& x = (scalar) i;
    const scalar& y = (scalar) j;
    scalar zz = scalarR*scalarR - x*x - y*y;
    if( zz < 0.0 ) continue;
    // Compute and normalize the distance
    scalar z = sqrt(zz)/R;
    assert( z >= 0.0 );
    assert( z <= 1.0 );

    // If the update will increase the value, do it
    marker_densities(row+i+1,col+j+1) = std::max(z,marker_densities(row+i+1,col+j+1));
  }
}


