#ifndef __STABLE_FLUIDS_SIM_H__
#define __STABLE_FLUIDS_SIM_H__

#include <iostream>

#include "FOSSFLOW/MathUtilities.h"

class StableFluidsSim
{
public:

  // Initialize the fluid simulation. Assumes rows == cols.
  //   rows: Number of rows in the Eulerian grid.
  //   cols: Number of cols in the Eulerian grid.
  //   diff: Diffusion coefficient for the passive markers.
  //   visc: Viscosity of the fluid. 
  StableFluidsSim( const int& rows, const int& cols, const scalar& diff = 0.0001, const scalar& visc = 0.00000001 );

  // Deallocates memory used during the simulation.
  ~StableFluidsSim();

  // Integrates the system forward in time by dt. 
  void stepSystem( const scalar& dt );
  void diffuse( int N, int b, ArrayXs* x, ArrayXs* x0, float diff, float dt );
  void advect( int N, int b, ArrayXs* d, ArrayXs* d0, ArrayXs* u, ArrayXs* v, float dt );
  void dens_step( int N, ArrayXs* x, ArrayXs* x0, ArrayXs* u, ArrayXs* v, float diff, float dt );
  void set_bnd( int N, int b, ArrayXs* x);
  void vel_step( int N, ArrayXs* u, ArrayXs* v, ArrayXs* u0, ArrayXs* v0, float visc, float dt );
  void project( int N, ArrayXs* u, ArrayXs* v, ArrayXs* p, ArrayXs* div );

  // Returns an array containing the marker densities. Note that the
  // boundary of this array is padded to handle boundary conditions. 
  const ArrayXs& getMarkerDensities() const;
  ArrayXs& getMarkerDensities();

  // Returns an array containing the horizontal components of the fluid velocity. 
  // Note that the boundary of this array is padded to handle boundary conditions. 
  ArrayXs& getHorizontalVelocities();

  // Returns an array containing the vertical components of the fluid velocity. 
  // Note that the boundary of this array is padded to handle boundary conditions. 
  ArrayXs& getVerticalVelocities();

  // Returns the number of non-boundary rows.
  int physicalRows() const;
  // Returns the number of non-boundary columns.
  int physicalCols() const;

  // Clears the density and velocity fields
  void clear();

  // STUDENTS: Feel free to add more member variables or functions as needed
  
private:
  
  // Diffusion coefficient for tracer particles
  scalar m_diff;
  // Viscosity of fluid
  scalar m_visc;
  // Width of the (square) grid
  int m_N;
  // Density of tracer particles
  ArrayXs* m_dens;
  // Horizontal velocities
  ArrayXs* m_u;
  // Vertical velocities
  ArrayXs* m_v;

  // STUDENTS: Feel free to add more member variables or functions as needed
  
};

#endif
