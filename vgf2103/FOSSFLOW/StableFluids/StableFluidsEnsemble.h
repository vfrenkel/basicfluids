#ifndef __STABLE_FLUIDS_ENSEMBLE_H__
#define __STABLE_FLUIDS_ENSEMBLE_H__

#include "FOSSFLOW/SimulationEnsemble.h"
#include "FOSSFLOW/MathUtilities.h"
#include "FOSSFLOW/RenderingUtilities.h"

#include "StableFluidsSim.h"
#include "Colormap.h"


class StableFluidsEnsemble : public SimulationEnsemble
{
public:

  StableFluidsEnsemble();

  virtual ~StableFluidsEnsemble();
  
  virtual void stepSystem( const scalar& dt );

  /////////////////////////////////////////////////////////////////////////////
  // Mouse and keyboard input handlers
  
  virtual void keyboard( const unsigned char& key, const int& x, const int& y );
  
  virtual void special( const int& key, const int& x, const int& y );
  
  virtual void mouse( const int& button, const int& state, const int& x, const int& y );

  virtual void motion( const int& x, const int& y );

  /////////////////////////////////////////////////////////////////////////////
  // Functions for use by OpenGL and GLUT
  
  virtual void initializeOpenGL();
  
  virtual void reshape( const int& w, const int& h );
  
  virtual void display();

  virtual unsigned int getGlutDisplayMode() const;
  
  virtual const int& getWindowWidth() const;
  
  virtual const int& getWindowHeight() const;
  
  virtual const renderingutils::Color& getBackgroundColor() const;

private:

  void addSphere( const int& row, const int& col, const int& R );

  StableFluidsSim m_fluid_sim;

  renderingutils::Color m_bgcolor;

  Colormap m_colormap;
  
  int m_window_width;
  int m_window_height;
  
  bool m_left_drag;
  bool m_right_drag;
  
  int m_last_row;
  int m_last_col;
};

#endif
