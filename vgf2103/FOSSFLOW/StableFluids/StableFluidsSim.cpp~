#include "StableFluidsSim.h"

#define IX(i, j) ((i)+(m_N+2)*(j))
#define SWAP(x0,x) {ArrayXs *tmp=x0;x0=x;x=tmp;}

StableFluidsSim::StableFluidsSim( const int& rows, const int& cols, const scalar& diff, const scalar& visc )
: m_diff(diff)
, m_visc(visc)
, m_N(rows)
, m_dens(new ArrayXs(m_N+2,m_N+2))
, m_u(new ArrayXs(m_N+2,m_N+2))
, m_v(new ArrayXs(m_N+2,m_N+2))
{
  assert(rows==cols);

  m_dens->setZero();
  m_u->setZero();
  m_v->setZero();
}

StableFluidsSim::~StableFluidsSim()
{
  if( m_dens != NULL )
  {
    delete m_dens;
    m_dens = NULL;
  }
  if( m_u != NULL )
  {
    delete m_u;
    m_u = NULL;
  }
  if( m_v != NULL )
  {
    delete m_v;
    m_v = NULL;
  }
}

void StableFluidsSim::stepSystem( const scalar& dt )
{
  ArrayXs u_prev = ArrayXs(*m_u);
  ArrayXs v_prev = ArrayXs(*m_v);
  ArrayXs dens_prev = ArrayXs(*m_dens);

  vel_step(m_N, m_u, m_v, &u_prev, &v_prev, m_visc, dt);
  dens_step(m_N, m_dens, &dens_prev, m_u, m_v, m_diff, dt);
}

void StableFluidsSim::diffuse( int N, int b, ArrayXs* x, ArrayXs* x0, float diff, float dt ) {
  //std::cout << "in diffuse" << std::endl;

  float a=dt*diff*N*N;

  for (int k = 0; k<20; k++) {
    for (int i = 1; i <= N; i++) {
      for (int j = 1; j <= N; j++) {
	(*x)(IX(i,j)) = ((*x0)(IX(i,j)) + a*((*x)(IX(i-1,j))+(*x)(IX(i+1,j))+
					     (*x)(IX(i,j-1))+(*x)(IX(i,j+1))))/(1+4*a);
      }
    }
    set_bnd( N, b, x );
  }

  //std::cout << "out diffuse" << std::endl;
}

void StableFluidsSim::advect( int N, int b, ArrayXs* d, ArrayXs* d0, ArrayXs* u, ArrayXs* v, float dt ) {
  //  std::cout << "in advect" << std::endl;

  int i0, j0, i1, j1;
  float x, y, s0, t0, s1, t1, dt0;

  dt0 = dt*N;
  for (int i=1; i<=N; i++) {
    for (int j=1; j<=N; j++) {
      x = i-dt0*(*u)(IX(i,j));
      y = j-dt0*(*v)(IX(i,j));
      if (x<0.5) x=0.5;
      if (x>N+0.5) {
	x=N+0.5;
      }
      i0=(int)x;
      i1=i0+1;
      if (y<0.5) y=0.5;
      if (y>N+0.5) {
	y=N+0.5;
      }
      j0=(int)y;
      j1=j0+1;
      s1=x-i0;
      s0=1-s1;
      t1=y-j0;
      t0=1-t1;
      //std::cout << "IN" << std::endl;
      (*d)(IX(i,j)) = s0*(t0*(*d0)(IX(i0,j0))+t1*(*d0)(IX(i0,j1)))+
	s1*(t0*(*d0)(IX(i1,j0))+t1*(*d0)(IX(i1,j1)));
      //      std::cout << "OUT" << std::endl;
    }
  }
  set_bnd ( N, b, d );

  //std::cout << "out advect" << std::endl;
}

void StableFluidsSim::dens_step( int N, ArrayXs* x, ArrayXs* x0, ArrayXs* u, ArrayXs* v, float diff, float dt ) {
  std::cout << "in dens_step" << std::endl;

  SWAP( x0, x );
  diffuse( N, 0, x, x0, diff, dt );
  SWAP( x0, x );
  advect( N, 0, x, x0, u, v, dt );

  std::cout << "out dens_step" << std::endl;
}

void StableFluidsSim::vel_step( int N, ArrayXs* u, ArrayXs* v, ArrayXs* u0, ArrayXs* v0, float visc, float dt ) {
  std::cout << "in vel_step" << std::endl;

  SWAP( u0, u );
  diffuse( N, 1, u, u0, visc, dt );
  SWAP( v0, v );
  diffuse( N, 2, v, v0, visc, dt );
  project( N, u, v, u0, v0 );
  SWAP( u0, u );
  SWAP( v0, v );
  advect( N, 1, u, u0, u0, v0, dt );
  advect( N, 2, v, v0, u0, v0, dt );
  project( N, u, v, u0, v0 );

  std::cout << "out vel_step" << std::endl;
}

void StableFluidsSim::project( int N, ArrayXs* u, ArrayXs* v, ArrayXs* p, ArrayXs* div ) {
  //std::cout << "in project" << std::endl;

  float h;

  h = 1.0/N;
  for(int i=1; i<=N; i++) {
    for(int j=1; j<=N; j++) {
      (*div)(IX(i,j)) = -0.5*h*((*u)(IX(i+1,j))-(*u)(IX(i-1,j))+
				(*v)(IX(i,j+1))-(*v)(IX(i,j-1)));
      (*p)(IX(i,j)) = 0;
    }
  }
  set_bnd( N, 0, div );
  set_bnd( N, 0, p );

  for(int k=0; k<20; k++) {
    for (int i=1; i<=N; i++) {
      for (int j=1; j<=N; j++) {
	(*p)(IX(i,j)) = ((*div)(IX(i,j))+(*p)(IX(i-1,j))+(*p)(IX(i+1,j))+
			 (*p)(IX(i,j-1))+(*p)(IX(i,j+1)))/4;
      }
    }
    set_bnd( N, 0, p );
  }

  for(int i=1; i<=N; i++) {
    for(int j=1; j<=N; j++) {
      (*u)(IX(i,j)) -= 0.5*((*p)(IX(i+1,j))-(*p)(IX(i-1,j)))/h;
      (*v)(IX(i,j)) -= 0.5*((*p)(IX(i,j+1))-(*p)(IX(i,j-1)))/h;
    }
  }
  set_bnd( N, 1, u );
  set_bnd( N, 2, v );

  //std::cout << "out project" << std::endl;
}

void StableFluidsSim::set_bnd( int N, int b, ArrayXs* x ) {
  //std::cout << "in set_bnd" << std::endl;

  for(int i=1; i<=N; i++) {
    (*x)(IX(0, i)) = b ==1 ? -(*x)(IX(1,i)) : (*x)(IX(1,i));
    (*x)(IX(N+1,i)) = b==1 ? -(*x)(IX(N,i)) : (*x)(IX(N,i));
    (*x)(IX(i,0)) = b == 2 ? -(*x)(IX(i,1)) : (*x)(IX(i,1));
    (*x)(IX(i,N+1)) = b==2 ? -(*x)(IX(i,N)) : (*x)(IX(i,N));
  }
  (*x)(IX(0, 0)) = 0.5*((*x)(IX(1,0)) + (*x)(IX(0,1)));
  (*x)(IX(0,N+1)) = 0.5*((*x)(IX(1,N+1)) + (*x)(IX(0,N)));
  (*x)(IX(N+1,0)) = 0.5*((*x)(IX(N,0)) + (*x)(IX(N+1,1)));
  (*x)(IX(N+1,N+1)) = 0.5*((*x)(IX(N,N+1)) + (*x)(IX(N+1,N)));

  //std::cout << "out set_bnd" << std::endl;
}

const ArrayXs& StableFluidsSim::getMarkerDensities() const
{
  return *m_dens;
}

ArrayXs& StableFluidsSim::getMarkerDensities()
{
  return *m_dens;
}

ArrayXs& StableFluidsSim::getHorizontalVelocities()
{
  return *m_u;
}

ArrayXs& StableFluidsSim::getVerticalVelocities()
{
  return *m_v;
}

int StableFluidsSim::physicalRows() const
{
  return m_N;
}

int StableFluidsSim::physicalCols() const
{
  return m_N;
}

void StableFluidsSim::clear()
{
  m_dens->setZero();
  m_u->setZero();
  m_v->setZero();
}


